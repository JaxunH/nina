//
// Created by ingram on 12/28/16.
// Copyright (c) 2016 Pairboost. All rights reserved.
//

import Foundation

private class Container {

  let channelDaemon:ChannelDaemon
  let videoDaemon:VideoDaemon
  let accountDaemon:AccountDaemon
  let facebookDaemon:FacebookDaemon

  init() {
    let pref = Pref()

    AlamofireLogger.shared.startLogging()
    BaseService.apiErrorHandler = toastApiErrorHandler;
    let channelService = ChannelService(pref)
    let videoService = VideoService(pref)
    let programService = ProgramService(pref)
    let accountService = AccountService(pref)

    self.channelDaemon = ChannelDaemon(channelService)
    self.videoDaemon = VideoDaemon(videoService, programService)
    self.accountDaemon = AccountDaemon(accountService, pref)
    self.facebookDaemon = FacebookDaemon(accountService, pref)
  }
}

class Injector {

  private static let container = Container()

  public static func inject<T>() -> T {
    if T.self == ChannelDaemon.self {
      return container.channelDaemon as! T
    } else if T.self == AccountDaemon.self {
      return container.accountDaemon as! T
    } else if T.self == VideoDaemon.self {
      return container.videoDaemon as! T
    } else if T.self == FacebookDaemon.self {
      return container.facebookDaemon as! T
    }
    fatalError("no instance registered as type: \(T.self)")
  }
}

