//
// Created by ingram on 12/30/16.
// Copyright (c) 2016 Pairboost. All rights reserved.
//

import Foundation

///
/// see XCode for Project -> Build Settings -> Other Swift Flags
///

class BuildConfig {
#if BUILD_DEBUG

  static let ottEndpoint = "http://eye-of-sauron.azurewebsites.net/"
  static let ottBrand = "saruman.ott"

  static let facebookAppId = "221604121624098"
  static let facebookClientToken = "2bd155f15e33bc47a462e1fe8b812ac2"
  static let facebookAppURLSchemeSuffix = ""

  static let enableLogger = true

#elseif BUILD_RELEASE

  //TODO
  static let ottEndpoint = "http://eye-of-sauron.azurewebsites.net/"

  //TODO
  static let ottBrand = "saruman.ott"

  //TODO after assign production facebookAppId, you need to add URL scheme `fbXXXXX` in Info.plist
  static let facebookAppId = "TODO"
  static let facebookClientToken = "TODO"
  static let facebookAppURLSchemeSuffix = ""

  static let enableLogger = true

#endif
}
