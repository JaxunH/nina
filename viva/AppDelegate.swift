//
//  AppDelegate.swift
//  viva
//
//  Created by ingram on 12/27/16.
//  Copyright © 2016 Pairboost. All rights reserved.
//

import UIKit
import FacebookCore
import SwiftTheme
import RxSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window:UIWindow?


  func application(_ application:UIApplication,
      didFinishLaunchingWithOptions launchOptions:[UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    // Override point for customization after application launch.
    window = UIWindow(frame: UIScreen.main.bounds)
    if let window = window {

      let rootViewController = CoverViewController()
      let navigationController = UINavigationController(rootViewController: rootViewController)
      navigationController.navigationBar.isTranslucent = false

      let tabBarFont = UIFont.preferredFont(forTextStyle: .body)
      UITabBarItem.appearance().setTitleTextAttributes([NSFontAttributeName: tabBarFont], for: .normal)
      UITabBarItem.appearance().titlePositionAdjustment = UIOffsetMake(0, -15)

      window.theme_backgroundColor = Themes.backgroundColor
      window.rootViewController = navigationController
      window.makeKeyAndVisible()

      SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    Observable.just(2).delay(RxTimeInterval(2), scheduler: MainScheduler()).subscribe(
            onNext: { success in
          if let navController:UINavigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController {

            var stack = navController.viewControllers
            stack.removeLast()
            stack.append(MainViewController())

            navController.setViewControllers(stack, animated: true)
          }
        }
            , onError: { error in }
        )

    //TODO: delay

    return true
  }

  func applicationWillResignActive(_ application:UIApplication) {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
  }

  func applicationDidEnterBackground(_ application:UIApplication) {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
  }

  func applicationWillEnterForeground(_ application:UIApplication) {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
  }

  func applicationDidBecomeActive(_ application:UIApplication) {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
  }

  func applicationWillTerminate(_ application:UIApplication) {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
  }

  func application(_ application:UIApplication, open url:URL, sourceApplication:String?, annotation:Any) -> Bool {
    let handled = SDKApplicationDelegate.shared.application(application,
        open: url,
        sourceApplication: sourceApplication,
        annotation: annotation)
    return handled
  }


}

