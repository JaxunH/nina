//
// Created by ingram on 12/30/16.
// Copyright (c) 2016 Pairboost. All rights reserved.
//

import Foundation

fileprivate enum LogLevel: String {
  case debug, info, event, warn, error
}

///
/// API similar to Willow Logger, but it does not support iOS8, so we copy its source code
///

class VivaLogger {

  private let enabled:Bool
  private let dateFormatter:DateFormatter
  init(enabled:Bool) {
    self.enabled = enabled
    self.dateFormatter = DateFormatter()
    self.dateFormatter.dateFormat = "MM-dd HH:mm:ss.SSS"
  }

#if DEBUG

  public func debug(file:String = #file,
      line:UInt = #line,
      function:String = #function,
      _ message:@autoclosure @escaping () -> String) {
    logMessage(file, line, function, message, with: LogLevel.debug)
  }

  public func info(file:String = #file,
      line:UInt = #line,
      function:String = #function,
      _ message:@autoclosure @escaping () -> String) {
    logMessage(file, line, function, message, with: LogLevel.info)
  }

  public func event(file:String = #file,
      line:UInt = #line,
      function:String = #function,
      _ message:@autoclosure @escaping () -> String) {
    logMessage(file, line, function, message, with: LogLevel.event)
  }

  public func warn(file:String = #file,
      line:UInt = #line,
      function:String = #function,
      _ message:@autoclosure @escaping () -> String) {
    logMessage(file, line, function, message, with: LogLevel.warn)
  }

  public func error(file:String = #file,
      line:UInt = #line,
      function:String = #function,
      _ message:@autoclosure @escaping () -> String) {
    logMessage(file, line, function, message, with: LogLevel.error)
  }

  public func debug(file:String = #file,
      line:UInt = #line,
      function:String = #function,
      _ message:@escaping () -> String) {
    logMessage(file, line, function, message, with: LogLevel.debug)
  }

  public func info(file:String = #file,
      line:UInt = #line,
      function:String = #function, _ message:@escaping () -> String) {
    logMessage(file, line, function, message, with: LogLevel.info)
  }

  public func event(file:String = #file,
      line:UInt = #line,
      function:String = #function, _ message:@escaping () -> String) {
    logMessage(file, line, function, message, with: LogLevel.event)
  }

  public func warn(file:String = #file,
      line:UInt = #line,
      function:String = #function, _ message:@escaping () -> String) {
    logMessage(file, line, function, message, with: LogLevel.warn)
  }

  public func error(file:String = #file,
      line:UInt = #line,
      function:String = #function, _ message:@escaping () -> String) {
    logMessage(file, line, function, message, with: LogLevel.error)
  }

#else

  public func debug(
      _ message:@autoclosure @escaping () -> String) {
    logMessage("", 0, "", message, with: LogLevel.debug)
  }

  public func info(
      _ message:@autoclosure @escaping () -> String) {
    logMessage("", 0, "", message, with: LogLevel.info)
  }

  public func event(
      _ message:@autoclosure @escaping () -> String) {
    logMessage("", 0, "", message, with: LogLevel.event)
  }

  public func warn(
      _ message:@autoclosure @escaping () -> String) {
    logMessage("", 0, "", message, with: LogLevel.warn)
  }

  public func error(
      _ message:@autoclosure @escaping () -> String) {
    logMessage("", 0, "", message, with: LogLevel.error)
  }

  public func debug(
      _ message:@escaping () -> String) {
    logMessage("", 0, "", message, with: LogLevel.debug)
  }

  public func info(
      _ message:@escaping () -> String) {
    logMessage("", 0, "", message, with: LogLevel.info)
  }

  public func event(
      _ message:@escaping () -> String) {
    logMessage("", 0, "", message, with: LogLevel.event)
  }

  public func warn(
      _ message:@escaping () -> String) {
    logMessage("", 0, "", message, with: LogLevel.warn)
  }

  public func error(
      _ message:@escaping () -> String) {
    logMessage("", 0, "", message, with: LogLevel.error)
  }

#endif

  private func logMessage(_ file:String,
      _ line:UInt,
      _ function:String,
      _ message:@escaping () -> String,
      with logLevel:LogLevel) {
    guard enabled else {
      return
    }
    logMessageIfAllowed(file, line, function, message, with: logLevel)
  }

  private func logMessageIfAllowed(_ file:String,
      _ line:UInt,
      _ function:String,
      _ message:() -> String,
      with logLevel:LogLevel) {
    guard logLevelAllowed(logLevel) else {
      return
    }
    logMessage(file, line, function, message(), with: logLevel)
  }

  private func logLevelAllowed(_ logLevel:LogLevel) -> Bool {
    return true
  }

  private func logMessage(_ file:String,
      _ line:UInt,
      _ function:String,
      _ message:String,
      with logLevel:LogLevel) {
    let name = file.components(separatedBy: "/").last ?? ""
    let now = dateFormatter.string(from: Date())
    print("\(now):[\(logLevel.rawValue.uppercased())] \(name):(\(line)) \(function): \(message)")
  }
}
