//
// Created by ingram on 1/3/17.
// Copyright (c) 2017 Pairboost. All rights reserved.
//

import Foundation
import FacebookCore
import RxSwift

class RegistrationError: Error {

}

class FacebookDaemon {

  private let pref:Pref
  private let accountService:AccountService

  public let readPermissions:[ReadPermission] = [.publicProfile, .email]

  init(_ accountService:AccountService, _ pref:Pref) {
    self.accountService = accountService
    self.pref = pref

    SDKSettings.appId = BuildConfig.facebookAppId
    SDKSettings.appURLSchemeSuffix = BuildConfig.facebookAppURLSchemeSuffix
    SDKSettings.clientToken = BuildConfig.facebookClientToken
  }

  /// throw RegistrationError
  private func getMe() -> Observable<FacebookUserDto> {
    return Observable.create({ observer in
      let connection = GraphRequestConnection()
      let parameters = [
          "fields": "id,name,gender,birthday,locale,email,picture.width(200).height(200)"
      ]
      connection.add(GraphRequest(graphPath: "/me", parameters: parameters)) { (httpResponse,
          result:GraphRequestResult) in
        switch result {
        case .success(let response):
          guard let json = response.dictionaryValue else {
            let regError = RegistrationError()
            log.warn("/me Graph Request empty result: \(regError)")
            observer.onError(regError)
            return
          }
          let facebookUser = FacebookUserDto.fromDictionary(json)
          log.debug("/me Graph Request Succeeded: \(response)")
          observer.onNext(facebookUser)
          observer.onCompleted()
        case .failed(let error):
          log.warn("/me Graph Request Failed: \(error)")
          observer.onError(RegistrationError())
        }
      }
      let disposable = Disposables.create {
        connection.cancel()
      }
      connection.start()

      return disposable
    }).observeOn(MainScheduler.instance)
  }

  /// throw RegistrationError
  func register(_ token:AccessToken) -> Observable<AccountDto> {
    return getMe().flatMap { facebookUser in
      return self.accountService.getTokenById(fbId: facebookUser.id,
          email: facebookUser.email,
          username: facebookUser.name,
          imagePath: facebookUser.pictureUrl)
    }.flatMap { token -> Observable<AccountDto> in
      self.pref.ottToken = token
      return self.accountService.getMyself()
    }
  }

}
