//
// Created by liq7 on 12/28/16.
// Copyright (c) 2016 Pairboost. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import RxCocoa
import RxSwift
import TTGSnackbar
import SwiftTheme

class SettingsController: UIViewController {

  required override init?(coder aDecoder:NSCoder) {
    super.init(coder: aDecoder)
  }

  required override init(nibName nibNameOrNil:String?, bundle nibBundleOrNil:Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    self.tabBarItem = UITabBarItem(title: "Settings", image: nil, selectedImage: nil)
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    let button = UIButton(type: .roundedRect)
    let buttonSwitch = UIButton(type: .roundedRect)
    let imageView = GradientView(startColor: UIColor.red, endColor: UIColor(hex: 0xaaaaaa), horizontalMode: true)
    button.setTitle("TODO registration", for: .normal)
    view.addSubview(button)

    buttonSwitch.setTitle("TODO switch theme", for: .normal)
    view.addSubview(buttonSwitch)

    view.addSubview(imageView)

    button.snp.makeConstraints { (make) -> Void in
      make.height.equalToSuperview().dividedBy(3)
      make.top.equalToSuperview()
      make.leading.trailing.equalToSuperview()
    }

    imageView.snp.makeConstraints({ (make) -> Void in
      make.height.equalToSuperview().dividedBy(3)
      make.bottom.equalTo(buttonSwitch.snp.top)
      make.top.equalTo(button.snp.bottom)
      make.leading.trailing.equalToSuperview()

    })

    buttonSwitch.snp.makeConstraints { (make) -> Void in
      make.height.equalToSuperview().dividedBy(3)
      make.bottom.equalToSuperview()
      make.leading.trailing.equalToSuperview()
    }
    buttonSwitch.titleLabel?.theme_backgroundColor = Themes.backgroundColor

    let tapper:Observable<Void> = button.rx.tap.asObservable()
    tapper.do(onNext: {
          button.isEnabled = false
        }).flatMap { [weak self] event -> Observable<Bool> in
          guard let safeSelf = self else {
            return Observable<Bool>.empty()
          }
          return FacebookLoginViewController.create { controller in
            safeSelf.present(controller, animated: true)
          }
        }.subscribe(
            onNext: { success in
              if success {
                TTGSnackbar(message: "TODO registered", duration: .middle).show()
              } else {
                TTGSnackbar(message: "TODO registration failed", duration: .middle).show()
              }
              button.isEnabled = true
            },
            onError: { error in button.isEnabled = true })
        .addDisposableTo(disposeBag)

    let switchTapper:Observable<Void> = buttonSwitch.rx.tap.asObservable()
    switchTapper.do(onNext: {

        }).subscribe(
            onNext: { Void in
              ThemeManager.setTheme(index: (ThemeManager.currentThemeIndex + 1) % (Themes.numberOfThemes))
            },
            onError: { error in
              print("error")
            }
        ).addDisposableTo(disposeBag)


  }


}
