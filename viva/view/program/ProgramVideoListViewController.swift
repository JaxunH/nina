//
// Created by liq on 2016/12/29.
// Copyright (c) 2016 Pairboost. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import RxSwift
import RxCocoa
import XLPagerTabStrip

struct VideoListConstants {
  static let pageSize = 10
  static let collectionViewSpacing:CGFloat = 5.0
  static let fullScreenSize = UIScreen.main.bounds.size
  static let column = 2
  static let itemWidth = (fullScreenSize.width - (collectionViewSpacing * CGFloat(2 + column - 1))) / CGFloat(
      column)
  static let itemHeight = (fullScreenSize.width - (collectionViewSpacing * CGFloat(2 + column - 1))) / CGFloat(
      column)
}

class ProgramVideoListViewController: UIViewController {

  private var programId:Int!
  fileprivate var containerHeight:CGFloat!
  fileprivate var videoTypeProgramDto:VideoTypeProgramDto!
  fileprivate let videoDaemon:VideoDaemon = Injector.inject()
  fileprivate let scrollSubject = PublishSubject<CGFloat>()
  fileprivate var videoProgramDtos = [VideoProgramDto]()
  fileprivate var collectionView:UICollectionView!
  fileprivate let flowLayout = VideoListUICollectionViewFlowLayout()

  required init?(coder aDecoder:NSCoder) {
    super.init(coder: aDecoder)
  }

  convenience init(programId pId:Int, videoTypeProgramDto dto:VideoTypeProgramDto) {
    self.init(programId: pId, videoTypeProgramDto: dto, headerHeight: 0.0)
  }

  init(programId pId:Int, videoTypeProgramDto dto:VideoTypeProgramDto, headerHeight height:CGFloat) {
    programId = pId
    videoTypeProgramDto = dto
    containerHeight = height
    super.init(nibName: nil, bundle: nil)
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    // above tab bar
    edgesForExtendedLayout = []

    collectionView = UICollectionView(frame: CGRect.zero,
        collectionViewLayout: flowLayout)
    collectionView.panGestureRecognizer
    collectionView.register(ProgramVideoCell.self, forCellWithReuseIdentifier: NSStringFromClass(ProgramVideoCell.self))
    collectionView.delegate = self
    collectionView.dataSource = self
    collectionView.backgroundColor = .clear

    view.addSubview(collectionView)

    collectionView.snp.makeConstraints { make in
      make.edges.equalTo(0)
    }
    fetchVideoList()
  }

  private func fetchVideoList() {
    videoDaemon.getVideosByProgramAndVideoType(programId: programId,
            pageNumber: 0,
            size: VideoListConstants.pageSize,
            videoTypeId: videoTypeProgramDto.videoTypeId)
        .subscribe(onNext: {
          [weak self] dtos in
          self?.videoProgramDtos += dtos
          self?.collectionView.reloadData()
        })
        .addDisposableTo(disposeBag)
  }

  func observerScroll() -> Observable<CGFloat> {
    return scrollSubject.asObservable()
  }
}

class VideoListUICollectionViewFlowLayout: UICollectionViewFlowLayout {

  required init?(coder aDecoder:NSCoder) {
    super.init(coder: aDecoder)
  }

  override init() {
    super.init()
    minimumInteritemSpacing = VideoListConstants.collectionViewSpacing
    minimumLineSpacing = VideoListConstants.collectionViewSpacing
    itemSize = CGSize(width: VideoListConstants.itemWidth,
        height: VideoListConstants.itemHeight)
    sectionInset = UIEdgeInsetsMake(VideoListConstants.collectionViewSpacing,
        VideoListConstants.collectionViewSpacing,
        VideoListConstants.collectionViewSpacing,
        VideoListConstants.collectionViewSpacing)
  }
}


extension ProgramVideoListViewController: UICollectionViewDelegate {

  func scrollViewDidScroll(_ scrollView:UIScrollView) {

    let passData:CGFloat!
    if collectionView.contentOffset.y <= containerHeight {
      passData = collectionView.contentOffset.y
    } else {
      passData = containerHeight
    }

    scrollSubject.onNext(passData)

    if (passData < 0) {
      return
    }

    flowLayout.sectionInset = UIEdgeInsetsMake(VideoListConstants.collectionViewSpacing + passData,
        VideoListConstants.collectionViewSpacing,
        VideoListConstants.collectionViewSpacing,
        VideoListConstants.collectionViewSpacing)
  }

  func collectionView(_ collectionView:UICollectionView, didSelectItemAt indexPath:IndexPath) {
    present(VivaAVPlayerViewController(videoId: videoProgramDtos[indexPath.row].videoId), animated: true)
  }


}

extension ProgramVideoListViewController: UICollectionViewDataSource {

  func collectionView(_ collectionView:UICollectionView, numberOfItemsInSection section:Int) -> Int {
    return videoProgramDtos.count
  }

  func collectionView(_ collectionView:UICollectionView, cellForItemAt indexPath:IndexPath) -> UICollectionViewCell {

    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(
        ProgramVideoCell.self),
        for: indexPath) as! ProgramVideoCell
    cell.update(videoProgramDtos[indexPath.row])
    return cell
  }

}

extension ProgramVideoListViewController: IndicatorInfoProvider {

  func indicatorInfo(for pagerTabStripController:PagerTabStripViewController) -> IndicatorInfo {
    return IndicatorInfo(title: videoTypeProgramDto.title)
  }

}

