//
// Created by liq on 2016/12/30.
// Copyright (c) 2016 Pairboost. All rights reserved.
//

import UIKit
import SnapKit
import Kingfisher
import SwiftTheme

class ProgramVideoCell: UICollectionViewCell {

  private let title = UILabel()
  private let episode = UILabel()
  private let duration = UILabelWithInset(CGRect.zero, top: 2.0, left: 5.0, bottom: 2.0, right: 5.0)
  private let thumbnail = UIImageView()

  required init?(coder aDecoder:NSCoder) {
    super.init(coder: aDecoder)
  }

  override init(frame:CGRect) {
    super.init(frame: frame)

    backgroundColor = UIColor.gray

    let bottomContainer = UIView()
    bottomContainer.backgroundColor = UIColor(hex: 0x000000, alpha: 0.2)

    title.font = UIFont.preferredFont(forTextStyle: .body)
    title.theme_textColor = Themes.themedTextColorLight
    episode.font = UIFont.preferredFont(forTextStyle: .caption1)
    episode.theme_textColor = Themes.themedTextColorDark
    duration.font = UIFont.preferredFont(forTextStyle: .body)
    duration.backgroundColor = UIColor.black
    duration.theme_textColor = Themes.themedTextColorLight
    thumbnail.contentMode = .scaleAspectFit

    addSubview(thumbnail)
    bottomContainer.addSubview(title)
    bottomContainer.addSubview(episode)
    addSubview(bottomContainer)
    addSubview(duration)

    duration.snp.makeConstraints { make in
      make.top.equalTo(6)
      make.trailing.equalTo(-6)
    }

    title.snp.makeConstraints { make in
      make.leading.trailing.equalTo(episode)
      make.bottom.equalTo(episode.snp.top)
    }

    episode.snp.makeConstraints { make in
      make.leading.equalTo(16)
      make.bottom.equalTo(-16)
      make.trailing.equalTo(-16)
    }

    thumbnail.snp.makeConstraints{ make in
      make.edges.equalTo(0)
    }

    bottomContainer.snp.makeConstraints { make in
      make.leading.trailing.bottom.equalTo(0)
      make.top.equalTo(title).offset(-16)
    }

  }

  func update(_ videoProgramDto:VideoProgramDto) {
    title.text = videoProgramDto.videoTitle
    episode.text = videoProgramDto.episode
    duration.text = videoProgramDto.duration
    thumbnail.kf.setImage(with: URL(string: videoProgramDto.thumbUrl))
  }
}
