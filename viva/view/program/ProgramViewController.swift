//
// Created by liq on 2016/12/29.
// Copyright (c) 2016 Pairboost. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import SnapKit

class ProgramViewController: UIViewController {

  private let videoDaemon:VideoDaemon = Injector.inject()
  private let accountDaemon:AccountDaemon = Injector.inject()

  private var tabControllers = [UIViewController]()
  private let programId:Int!
  private var programDto:ProgramDto?
  private var isProgramSubscribe:Bool = false
  private var subscribeSubject:PublishSubject<Bool>?

  private let programTitle = UILabel()
  private let videoCount = UILabel()
  private let type = UILabel()
  private let participants = UILabel()
  private let programDescription = UILabel()
  private let tabContainer = UIView()
  private let scrollView = UIScrollView()
  private let contentScrollView = UIView()
  private var rightButton:UIBarButtonItem?

  fileprivate let headerContentContainer = UIView()

  required init?(coder aDecoder:NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  init(programId id:Int) {
    programId = id
    super.init(nibName: nil, bundle: nil)
  }

  override
  func viewDidLoad() {
    super.viewDidLoad()

    view.backgroundColor = UIColor(hex: 0x303030)

    participants.lineBreakMode = .byWordWrapping
    participants.numberOfLines = 0

    headerContentContainer.addSubview(programTitle)
    headerContentContainer.addSubview(videoCount)
    headerContentContainer.addSubview(type)
    headerContentContainer.addSubview(participants)
    headerContentContainer.addSubview(programDescription)

    scrollView.addSubview(contentScrollView)
    contentScrollView.addSubview(headerContentContainer)
    contentScrollView.addSubview(tabContainer)
    view.addSubview(scrollView)

    scrollView.snp.makeConstraints { make in
      make.edges.equalTo(view)
    }

    contentScrollView.snp.makeConstraints { make in
      make.width.equalTo(scrollView)
      make.edges.equalTo(scrollView)
      make.bottom.equalTo(tabContainer)
    }

    headerContentContainer.snp.makeConstraints { make in
      make.top.equalTo(0)
      make.leading.equalTo(0)
      make.trailing.equalTo(0)
      make.bottom.equalTo(programDescription)
    }

    tabContainer.snp.makeConstraints { make in
      make.top.equalTo(headerContentContainer.snp.bottom)
      make.height.equalTo(view)
      make.leading.equalTo(0)
      make.trailing.equalTo(0)
    }

    programTitle.snp.makeConstraints { make in
      make.top.equalTo(6)
      make.leading.equalTo(6)
      make.trailing.equalTo(-6)
    }

    videoCount.snp.makeConstraints { make in
      make.top.equalTo(programTitle.snp.bottom).offset(6)
      make.leading.trailing.equalTo(programTitle)
    }

    type.snp.makeConstraints { make in
      make.top.equalTo(videoCount.snp.bottom).offset(6)
      make.leading.trailing.equalTo(programTitle)
    }

    participants.snp.makeConstraints { make in
      make.top.equalTo(type.snp.bottom).offset(6)
      make.leading.trailing.equalTo(programTitle)
    }

    programDescription.snp.makeConstraints { make in
      make.top.equalTo(participants.snp.bottom).offset(6)
      make.leading.trailing.equalTo(programTitle)
    }

    fetchProgram()
    fetchSubscribe()
  }

  private func fetchProgram() {
    videoDaemon.getProgram(programId)
        .flatMap { [weak weakSelf = self] dto -> Observable<Array<VideoTypeProgramDto>> in
          guard let strongSelf = weakSelf  else {
            return Observable.just([])
          }
          strongSelf.bindProgram(dto)
          return strongSelf.videoDaemon.getVideoTypes()
        }
        .subscribe(onNext: bindTab)
        .addDisposableTo(disposeBag)
  }

  private func fetchSubscribe() {
    accountDaemon.isSubscribe(programId)
        .subscribe(onNext: { [weak weakSelf = self] isProgramSubscribe in
          guard let strongSelf = weakSelf else {
            return
          }
          strongSelf.isProgramSubscribe = isProgramSubscribe
          strongSelf.bindSubscribe()
        })
        .addDisposableTo(disposeBag)
  }

  @objc private func clickSubscribe() {
    FacebookLoginViewController.create { [weak self] controller in
          self?.present(controller, animated: true)
        }
        .subscribe(onNext: { [weak self] success in
          guard let strongSelf = self else {
            return
          }
          if success {
            strongSelf.isProgramSubscribe = !strongSelf.isProgramSubscribe
            strongSelf.bindSubscribeImg()
            if strongSelf.subscribeSubject == nil {
              strongSelf.createSubscribeSubject()
            }
            strongSelf.subscribeSubject?.onNext(strongSelf.isProgramSubscribe)
          }
        })
        .addDisposableTo(disposeBag)
  }

  func bindSubscribe() {
    let icon = isProgramSubscribe ? UIImage(imageLiteralResourceName: "icon_favorite") : UIImage(
        imageLiteralResourceName: "icon_unfavorite")

    rightButton = UIBarButtonItem(image: icon,
        style: .plain,
        target: self,
        action: #selector(ProgramViewController.clickSubscribe))
    navigationItem.rightBarButtonItem = rightButton
  }

  private func createSubscribeSubject() {
    subscribeSubject = PublishSubject<Bool>()
    subscribeSubject!.debounce(0.4, scheduler: MainScheduler.instance)
        .flatMap { [weak weakSelf = self] willProgramSubscribe -> Observable<Void> in
          guard let strongSelf = weakSelf else {
            return Observable.empty()
          }
          if willProgramSubscribe {
            return strongSelf.accountDaemon.subscribe(strongSelf.programId)
          } else {
            return strongSelf.accountDaemon.unsubscribe(strongSelf.programId)
          }
        }
        .subscribe()
        .addDisposableTo(disposeBag)
  }

  func bindSubscribeImg() {
    rightButton?.image = isProgramSubscribe ? UIImage(imageLiteralResourceName: "icon_favorite") : UIImage(
        imageLiteralResourceName: "icon_unfavorite")
  }

  private func bindProgram(_ programDto:ProgramDto) {
    self.programDto = programDto
    programTitle.text = programDto.title
    type.text = programDto.genre ?? "none"

    var participantTotal = String()
    let colon = ":"
    let comma = " , "
    for participant in programDto.participants {
      participantTotal += comma
      participantTotal += participant.type
      participantTotal += colon
      participantTotal += participant.name
    }
    if participantTotal.characters.count > comma.characters.count {
      let range = participantTotal.startIndex ... participantTotal.index(participantTotal.startIndex,
          offsetBy: (comma.characters.count - 1))
      participantTotal.removeSubrange(range)
    }
    participants.text = participantTotal
    programDescription.text = programDto.programDescription
  }

  private func bindTab(_ dtos:[VideoTypeProgramDto]) {
    view.layoutIfNeeded()

    let controller:ProgramTabViewController = ProgramTabViewController(videoTypesProgramDtos: dtos,
        programId: programId, headerHeight: headerContentContainer.bounds.size.height)

    controller.observerScroll()
        .subscribe(onNext: { [weak weakSelf = self] contentY in
          guard let strongSelf = weakSelf else {
            return
          }
          strongSelf.scrollView.scrollRectToVisible(CGRect(origin: CGPoint(x: 0, y: contentY),
              size: strongSelf.scrollView.frame.size),
              animated: false)

        })
        .addDisposableTo(disposeBag)
    addChildViewController(controller)
    didMove(toParentViewController: controller)

    tabContainer.addSubview(controller.view)
  }
}