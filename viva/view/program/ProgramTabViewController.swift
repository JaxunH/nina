//
// Created by liq on 2017/1/5.
// Copyright (c) 2017 Pairboost. All rights reserved.
//

import Foundation
import XLPagerTabStrip
import RxSwift

class ProgramTabViewController: XLPagerTabStrip.ButtonBarPagerTabStripViewController {

  private var videoTypeProgramDto = [VideoTypeProgramDto]()
  private var programId = -1
  private var headerHeight:CGFloat = 0.0
  private let scrollSubject = PublishSubject<CGFloat>()

  init(videoTypesProgramDtos dtos:[VideoTypeProgramDto], programId id:Int, headerHeight height:CGFloat) {
    videoTypeProgramDto = dtos
    programId = id
    headerHeight = height;
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder aDecoder:NSCoder) {
    super.init(coder: aDecoder)
  }

  override public func viewControllers(for pagerTabStripController:PagerTabStripViewController) -> [UIViewController] {

    var controllers = [UIViewController]()

    for dto in videoTypeProgramDto {
      let vc = ProgramVideoListViewController(programId: programId,
          videoTypeProgramDto: dto,
          headerHeight: headerHeight)
      controllers.append(vc)
      vc.observerScroll().subscribe(onNext: { [weak weakSelf = self] contentY in
            weakSelf?.scrollSubject.onNext(contentY)
          }).addDisposableTo(disposeBag)
    }
    return controllers
  }

  func observerScroll() -> Observable<CGFloat> {
    return scrollSubject.asObservable()
  }

}
