//
// Created by liq on 2017/1/9.
// Copyright (c) 2017 Pairboost. All rights reserved.
//

import UIKit

extension String {

  func heightForComment(font:UIFont, width:CGFloat) -> CGFloat {

    let rect = NSString(string: self).boundingRect(with: CGSize(width: width, height: CGFloat(MAXFLOAT)),
        options: .usesLineFragmentOrigin,
        attributes: [NSFontAttributeName: font],
        context: nil)

    return ceil(rect.height)
    
  }

}
