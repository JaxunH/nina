//
// Created by liq7 on 1/9/17.
// Copyright (c) 2017 Pairboost. All rights reserved.
//

import Foundation
import UIKit
import XLPagerTabStrip
import SnapKit

class GradientButtonBarPagerTabStripViewController: ButtonBarPagerTabStripViewController {


  override func viewDidLoad() {
    super.viewDidLoad()
    let gradientView = GradientView(startColor: UIColor.black, endColor: UIColor(hex: 0xaaaaaa), horizontalMode: true)
    gradientView.frame = self.buttonBarView.frame
    var setting:ButtonBarPagerTabStripSettings = self.settings
    setting.style.buttonBarBackgroundColor = UIColor.clear
    setting.style.buttonBarItemBackgroundColor = UIColor.clear
    self.settings = setting

    let bar:ButtonBarView = self.buttonBarView
    bar.backgroundView = gradientView

    print(self.buttonBarView.subviews)
  }


}