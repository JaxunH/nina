//
// Created by liq7 on 12/29/16.
// Copyright (c) 2016 Pairboost. All rights reserved.
//

import Foundation
import UIKit

extension UIKit.UICollectionViewCell{

  func estimateHeight() -> CGFloat {
    updateConstraintsIfNeeded()
    layoutIfNeeded()

    return self.contentView.systemLayoutSizeFitting( UILayoutFittingCompressedSize).height

  }

  func estimateWidth() -> CGFloat {
    updateConstraintsIfNeeded()
    layoutIfNeeded()

    return self.contentView.systemLayoutSizeFitting( UILayoutFittingCompressedSize).width

  }
}