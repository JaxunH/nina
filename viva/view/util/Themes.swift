//
// Created by liq7 on 1/4/17.
// Copyright (c) 2017 Pairboost. All rights reserved.
//

import Foundation
import UIKit
import SwiftTheme
struct Themes {
  static let lightTextColor = UIColor.white
  static let darkTextColor = UIColor.lightGray

  static let backgroundColor = ThemeColorPicker.pickerWithColors(["#fff", "#888", "#000"])
  static let backgroundColorCell = ThemeColorPicker.pickerWithColors(["#fff", "#888", "#000"])
  static let themedTextColorLight = ThemeColorPicker.pickerWithColors(["#000", "#000", "#fff"])
  static let themedTextColorDark = ThemeColorPicker.pickerWithColors(["#fff", "#fff", "#000"])
  static let numberOfThemes = 3
  static let separatorColor =  ThemeColorPicker.pickerWithColors(["#000", "#000", "#fff"])

  static let coverBackgroundColor = UIColor.orange
}