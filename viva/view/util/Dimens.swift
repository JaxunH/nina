//
// Created by liq7 on 1/4/17.
// Copyright (c) 2017 Pairboost. All rights reserved.
//

import Foundation
struct Dimens {
  static let MATERIAL_SPACING_4 = 4
  static let MATERIAL_SPACING_8 = 8
  static let MATERIAL_SPACING_16 = 16
  static let MATERIAL_SPACING_24 = 24
  static let MATERIAL_SPACING_48 = 48
}