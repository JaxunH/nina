//
// Created by liq7 on 1/6/17.
// Copyright (c) 2017 Pairboost. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class CoverViewController: UIKit.UIViewController {
  override func viewDidLoad() {
    super.viewDidLoad()
    self.view.backgroundColor = Themes.coverBackgroundColor

    let indicatorView:UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
    self.view.addSubview(indicatorView)
    indicatorView.snp.makeConstraints { (make) -> Void in
      make.center.equalToSuperview()
    }
    indicatorView.startAnimating()
  }

  required override init?(coder aDecoder:NSCoder) {
    super.init(coder: aDecoder)
  }

  required override init(nibName nibNameOrNil:String?, bundle nibBundleOrNil:Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
  }
}