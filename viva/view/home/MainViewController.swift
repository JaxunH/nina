//
//  ViewController.swift
//  viva
//
//  Created by ingram on 12/27/16.
//  Copyright © 2016 Pairboost. All rights reserved.
//

import UIKit
import SnapKit
import AVKit
import AVFoundation

class MainViewController: UITabBarController, UITabBarControllerDelegate {

  let channelDaemon:ChannelDaemon = Injector.inject()

  override func viewDidLoad() {
    super.viewDidLoad()
    navigationItem.setHidesBackButton(true, animated: false)

    let audioSession = AVAudioSession.sharedInstance()
    do {
      try audioSession.setCategory(AVAudioSessionCategoryPlayback)
    } catch {
      print("Setting category to AVAudioSessionCategoryPlayback failed.")
    }

    view.backgroundColor = UIColor.blue;
    let box = UIView();
    box.backgroundColor = UIColor.red;
//    view.addSubview(box);
//
//    box.snp.makeConstraints { (make) -> Void in
//      make.top.equalTo(view).offset(20)
//      make.left.equalTo(view).offset(20)
//      make.bottom.equalTo(view).offset(-20)
//      make.right.equalTo(view).offset(-20)
//    }

//    let button = UIButton();
//    view.addSubview(button)
//    button.setTitle("Play", for: .normal)
//    button.snp.makeConstraints { (make) -> Void in
//      make.bottom.equalTo(view).offset(-30)
//      make.height.equalTo(50)
//      make.left.equalTo(view).offset(20)
//      make.right.equalTo(view).offset(-20)
//    }
//    button.backgroundColor = UIColor.brown
//    button.addTarget(self, action: Selector("play:"), for: .touchUpInside)
    initTabBar()
  }

  func initTabBar() {
    let hotVc = ExploreController(nibName: nil, bundle: nil)
    hotVc.view.backgroundColor = UIColor.brown

    let settingsVc = SettingsController(nibName: nil, bundle: nil)
    settingsVc.view.backgroundColor = UIColor.blue
    self.viewControllers = [hotVc, settingsVc]

  }

  func play(_ sender:AnyObject?) {
    guard let url = URL(string: "https://devimages.apple.com.edgekey.net/samplecode/avfoundationMedia/AVFoundationQueuePlayer_HLS2/master.m3u8") else {
      return
    }

    // Create an AVPlayer, passing it the HTTP Live Streaming URL.
    let player = AVPlayer(url: url)

    // Create a new AVPlayerViewController and pass it a reference to the player.
    let controller = AVPlayerViewController()
    controller.player = player

    // Modally present the player and call the player's play() method when complete.
    present(controller, animated: true) {
      player.play()
    }
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }


  func statusBarHeight() -> CGFloat {
    let statusBarSize = UIApplication.shared.statusBarFrame.size
    return Swift.min(statusBarSize.width, statusBarSize.height)
  }

}

