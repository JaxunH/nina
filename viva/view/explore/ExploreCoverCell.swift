//
// Created by liq7 on 1/3/17.
// Copyright (c) 2017 Pairboost. All rights reserved.
//

import Foundation
import SnapKit
import UIKit
import Kingfisher

class ExploreCoverCell: UIKit.UICollectionViewCell {
  private var coverImageView:UIImageView
  public var coverMask:UIView


  override init(frame:CGRect) {
    coverImageView = UIImageView()
    coverMask = UIView()

    super.init(frame: frame)
    contentView.backgroundColor = UIColor.darkGray
    contentView.addSubview(coverImageView)
    contentView.addSubview(coverMask)


    coverImageView.snp.makeConstraints { (make) -> Void in
      make.edges.equalToSuperview()
      make.height.equalTo(150)
    }
    coverMask.snp.makeConstraints { (make) -> Void in
      make.edges.equalToSuperview()
      make.height.equalTo(150)
    }

    coverMask.alpha = 0.0
    coverMask.backgroundColor = UIColor.cyan
  }

  required init(coder aDecoder:NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func update(_ imageUrl:URL) {
    coverImageView.kf.setImage(with: imageUrl)
  }


}
