//
// Created by liq7 on 12/28/16.
// Copyright (c) 2016 Pairboost. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import RxSwift
import RxCocoa
import Kingfisher
import SwiftTheme

public struct HotProgramSize {
  static var iconHeight = CGFloat(150)
  static var imagePadding = Dimens.MATERIAL_SPACING_16
  static var titlePadding = Dimens.MATERIAL_SPACING_16
  static var titleFont = UIFont.systemFont(ofSize: UIFont.systemFontSize)

  static func getSize(text:String, width:CGFloat) -> CGSize {
    let height = CGFloat(imagePadding) + iconHeight + text.heightForComment(font: titleFont, width: width) + CGFloat(
        titlePadding) * 2
    return CGSize(width: width, height: height)
  }
}

class HotProgramCell: UIKit.UICollectionViewCell {
  private var title:UILabel
  private var icon:UIImageView


  override init(frame:CGRect) {
    title = UILabel()
    icon = UIImageView()
    super.init(frame: frame)
    contentView.theme_backgroundColor = Themes.backgroundColorCell
    contentView.addSubview(title)
    contentView.addSubview(icon)

    title.theme_textColor = Themes.themedTextColorLight

    icon.snp.makeConstraints({ (make) -> Void in
      make.top.equalToSuperview().offset(HotProgramSize.imagePadding)
      make.left.equalToSuperview().offset(HotProgramSize.imagePadding)
      make.right.equalToSuperview().offset(-HotProgramSize.imagePadding)
      make.height.equalTo(HotProgramSize.iconHeight)
    })

    title.snp.makeConstraints { (make) -> Void in
      make.top.equalTo(icon.snp.bottom).offset(HotProgramSize.titlePadding)
      make.left.equalToSuperview().offset(HotProgramSize.titlePadding)
      make.right.equalToSuperview().offset(-HotProgramSize.titlePadding)
      make.bottom.equalToSuperview().offset(-HotProgramSize.titlePadding)
    }

  }

  required init(coder aDecoder:NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func update(_ dto:ProgramHotDto) {
    title.text = "Program \(dto.programTitle)"
    icon.kf.setImage(with: URL(string: dto.programCoverUrl))

  }


}
