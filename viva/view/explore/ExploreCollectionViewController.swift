//
// Created by liq7 on 12/28/16.
// Copyright (c) 2016 Pairboost. All rights reserved.
//


import SnapKit
import Foundation
import UIKit
import XLPagerTabStrip
import RxSwift
import AVKit
import AVFoundation
import SwiftTheme

enum Const {
  static let PILL_VIEW_HEIGHT:CGFloat = 40
}

class ExploreCollectionViewController: UICollectionViewController, IndicatorInfoProvider, UICollectionViewDelegateFlowLayout, SelectCategoryDelegate {

  private var page:String = ""
  let channelDaemon:ChannelDaemon = Injector.inject()
  let videoDaemon:VideoDaemon = Injector.inject()
  private var channelDto:ChannelDto?
  private var items:[Any]
  private var categoryPillView:CategoryPillCollectionView
  private var selectedCategory:CategoryDto?

  private var contentOffsetY:CGFloat = 0

  func indicatorInfo(for pagerTabStripController:PagerTabStripViewController) -> IndicatorInfo {
    return IndicatorInfo(title: String(format: "%@", page))
  }

  func setPage(_ index:String) {
    self.page = index

  }

  convenience override init(nibName nibNameOrNil:String?, bundle nibBundleOrNil:Bundle?) {
    self.init(collectionViewLayout: UICollectionViewFlowLayout.init())
    return
  }


  convenience required init?(coder aDecoder:NSCoder) {
    self.init(collectionViewLayout: UICollectionViewFlowLayout.init())
    return
  }

  override init(collectionViewLayout layout:UICollectionViewLayout) {
    items = []
    categoryPillView = CategoryPillCollectionView(frame: CGRect.zero)
    super.init(collectionViewLayout: layout)
  }


  override func viewDidLoad() {
    super.viewDidLoad()
    initViews()
    print("viewDidload \(page)")
    reloadVideos()
    contentOffsetY = 0
  }


  func initViews() {
    if let cv:UICollectionView = collectionView {
      cv.snp.makeConstraints { (make) -> Void in
        make.edges.equalToSuperview();
      }
      cv.delegate = self
      cv.register(HotVideoCell.self, forCellWithReuseIdentifier: NSStringFromClass(HotVideoCell.self))
      cv.register(HotProgramCell.self, forCellWithReuseIdentifier: NSStringFromClass(HotProgramCell.self))
      cv.register(ExploreHeaderCell.self, forCellWithReuseIdentifier: NSStringFromClass(ExploreHeaderCell.self))
      cv.register(ExploreCoverCell.self, forCellWithReuseIdentifier: NSStringFromClass(ExploreCoverCell.self))
      cv.register(EmptyCell.self, forCellWithReuseIdentifier: NSStringFromClass(EmptyCell.self))

      self.view.insertSubview(categoryPillView, aboveSubview: cv)
      categoryPillView.snp.makeConstraints({ (make) -> Void in
        make.height.equalTo(Const.PILL_VIEW_HEIGHT)
        make.leading.equalToSuperview()
        make.top.equalToSuperview()
        make.trailing.equalToSuperview()
      })

      cv.theme_backgroundColor = Themes.separatorColor

      categoryPillView.selectCategoryDelegate = self

    } else {
      return
    }
  }


  override func collectionView(_ collectionView:UICollectionView, numberOfItemsInSection section:Int) -> Int {
    return self.items.count;
  }

  override func collectionView(_ collectionView:UICollectionView, didSelectItemAt indexPath:IndexPath) {
    switch items[indexPath.row] {
    case is VideoHotDto:
      let dto = items[indexPath.row] as! VideoHotDto
      present(VivaBMPlayerViewController(videoHotDto: dto), animated: true)
      break

    case is ProgramHotDto:
      let dto = items[indexPath.row] as! ProgramHotDto
      let controller:ProgramViewController = ProgramViewController(programId: dto.programId)
      self.navigationController?.pushViewController(controller, animated: true)
      break

    default:
      break
    }
  }

  func collectionView(_ collectionView:UICollectionView,
      layout collectionViewLayout:UICollectionViewLayout,
      sizeForItemAt indexPath:IndexPath) -> CGSize {
    switch items[indexPath.row] {
    case is VideoHotDto:
      let dto = items[indexPath.row] as! VideoHotDto
      return HotVideoCellSize.getSize(text: dto.videoTitle, width: collectionView.bounds.width)

    case is ProgramHotDto:
      let dto = items[indexPath.row] as! ProgramHotDto
      return HotProgramSize.getSize(text: dto.programTitle, width: collectionView.bounds.width)

    case is ExploreHeader:
      let dto = items[indexPath.row] as! ExploreHeader
      return ExploreHeaderSize.getSize(text: dto.title, width: collectionView.bounds.width)

    case is URL:
      return CGSize(width: collectionView.bounds.width, height: 150)

    case is String:
      return CGSize(width: collectionView.bounds.width, height: collectionView.frame.height)

    default:
      return CGSize.zero
    }
  }

  override func collectionView(_ collectionView:UICollectionView,
      cellForItemAt indexPath:IndexPath) -> UICollectionViewCell {
    var cell:UICollectionViewCell
    switch items[indexPath.row] {
    case is VideoHotDto:
      cell = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(HotVideoCell.self),
          for: indexPath) as! HotVideoCell
      (cell as! HotVideoCell).update(items[indexPath.row] as! VideoHotDto)
      break
    case is ProgramHotDto:
      cell = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(HotProgramCell.self),
          for: indexPath) as! HotProgramCell
      (cell as! HotProgramCell).update(items[indexPath.row] as! ProgramHotDto)
      break
    case is ExploreHeader:
      cell = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(ExploreHeaderCell.self),
          for: indexPath) as! ExploreHeaderCell
      (cell as! ExploreHeaderCell).update(items[indexPath.row] as! ExploreHeader)
      break
    case is URL:
      cell = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(ExploreCoverCell.self),
          for: indexPath) as! ExploreCoverCell
      (cell as! ExploreCoverCell).update(items[indexPath.row] as! URL)
      break
    case is String:
      cell = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(EmptyCell.self),
          for: indexPath) as! EmptyCell
      break
    default:
      cell = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(HotProgramCell.self),
          for: indexPath) as! HotVideoCell
    }
    return cell
  }

  func setDto(_ dto:ChannelDto) {
    self.channelDto = dto
  }

  func reloadVideos() {
    self.items.removeAll()
    if let dto = channelDto {
      categoryPillView.setCategories(dto.categories)

      Observable.zip(
              channelDaemon.getHotPrograms(channelId: dto.channelId,
                  pageNumber: 0,
                  size: channelDaemon.PAGE_SIZE,
                  categoryId: self.selectedCategory?.categoryId),
              channelDaemon.getHotVideos(channelId: dto.channelId,
                  pageNumber: 0,
                  size: channelDaemon.PAGE_SIZE,
                  categoryId: self.selectedCategory?.categoryId),
              channelDaemon.getLatestVideos(channelId: dto.channelId,
                  pageNumber: 0,
                  size: channelDaemon.PAGE_SIZE,
                  categoryId: self.selectedCategory?.categoryId)
          ) {
            (hotPrograms:[ProgramHotDto], hotVideos:[VideoHotDto], latestVideos:[VideoHotDto]) -> Int in
            self.items = self.items + [URL(string: "https://dummyimage.com/480x200/000/fff")]

            self.items = self.items + [ExploreHeader("Hot Programs")]
            self.items = self.items + hotPrograms as [Any]
            self.items = self.items + [ExploreHeader("Hot Videos")]
            self.items = self.items + hotVideos as [Any]
            self.items = self.items + [ExploreHeader("New Videos")]
            self.items = self.items + latestVideos as [Any]

            return (hotPrograms.count + hotVideos.count + latestVideos.count)
          }.subscribe {
            if ($0.element == 0) {
              self.items.removeAll()
              self.items.append("EMPTY")
            }

            if let c = self.collectionView {
              c.reloadData()
            }
          }
    } else {
      print("error")
      return
    }
  }

  override func scrollViewDidScroll(_ scrollView:UIScrollView) {
    self.contentOffsetY = scrollView.contentOffset.y

    if let c:UICollectionView = self.collectionView {
      let coverHeightForCalc:CGFloat = 150 - Const.PILL_VIEW_HEIGHT

      if let coverCell:ExploreCoverCell = c.cellForItem(at: IndexPath(item: 0, section: 0)) as! ExploreCoverCell? {
        let alpha:CGFloat = ((coverHeightForCalc - self.contentOffsetY) / coverHeightForCalc)
        coverCell.coverMask.alpha = 1 - alpha
        let color:UIColor = coverCell.coverMask.backgroundColor!
        if coverCell.coverMask.alpha >= 1 {
          self.categoryPillView.backgroundColor = UIColor.cyan
        } else {
          self.categoryPillView.backgroundColor = UIColor.clear
        }
      }
    }
  }

  func resetCategory() {
    selectedCategory = nil
    categoryPillView.resetCategory()
  }

  func selectCategory(_ categoryDto:CategoryDto) {
    selectedCategory = categoryDto
    reloadVideos()
  }

  func collectionView(_ collectionView:UICollectionView,
      layout collectionViewLayout:UICollectionViewLayout,
      minimumLineSpacingForSectionAt section:Int) -> CGFloat {
    return 0.5
  }


}
