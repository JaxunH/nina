//
// Created by liq7 on 12/28/16.
// Copyright (c) 2016 Pairboost. All rights reserved.
//

import Foundation
import UIKit
import XLPagerTabStrip
import RxSwift

class ExploreController: GradientButtonBarPagerTabStripViewController {

  var views:[UIViewController]
  var channels:[ChannelDto]
  let channelDaemon:ChannelDaemon = Injector.inject()

  required init?(coder aDecoder:NSCoder) {
    views = [UIViewController]()
    channels = [ChannelDto]()
    super.init(coder: aDecoder)
  }

  required override init(nibName nibNameOrNil:String?, bundle nibBundleOrNil:Bundle?) {
    views = [UIViewController]()
    views.append(ExploreCollectionViewController(collectionViewLayout: UICollectionViewFlowLayout.init()))
    channels = [ChannelDto]()
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
//    settings.style.segmentedControlColor = .white
    self.tabBarItem = UITabBarItem(title: "Hot", image: nil, selectedImage: nil)
    edgesForExtendedLayout = []
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    loadChannels()
  }


  override public func viewControllers(for pagerTabStripController:PagerTabStripViewController) -> [UIViewController] {

    channels.forEach { dto in
      let vc = ExploreCollectionViewController(collectionViewLayout: UICollectionViewFlowLayout.init())
      vc.setPage(dto.channelTitle)
      vc.setDto(dto)
      views.append(vc)
    }

    return views
  }

  override func collectionView(_ collectionView:UICollectionView, didSelectItemAt indexPath:IndexPath) {
    super.collectionView(collectionView, didSelectItemAt: indexPath)

    if let prev:UIKit.UIViewController = views[max(0, indexPath.row - 1)] {
      let _ = prev.view
    }

    if let next:UIKit.UIViewController = views[min(indexPath.row + 1, views.count - 1)] {
      let _ = next.view
    }

    let channelView:ExploreCollectionViewController = views[indexPath.row] as! ExploreCollectionViewController
    channelView.resetCategory()

  }

  func loadChannels() {
    channelDaemon.getChannels().subscribe(onNext: { (dtos:[ChannelDto]) -> Void in
          self.channels = dtos
          self.views.removeAll()
          self.reloadPagerTabStripView()
        })
  }
}
