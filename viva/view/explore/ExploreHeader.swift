//
// Created by liq7 on 1/3/17.
// Copyright (c) 2017 Pairboost. All rights reserved.
//

import Foundation

class ExploreHeader: JsonDto {
  public let title:String

  required init(_ title:String) {
    self.title = title
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {
    return self.init("Not implemented")
  }
  var hashValue:Int {
    return self.title.hashValue
  }

  static func ==(lhs:ExploreHeader, rhs:ExploreHeader) -> Bool {
    return lhs.title == rhs.title
  }
  var description:String {
    return "ExploreHeader: \(title)"
  }

}