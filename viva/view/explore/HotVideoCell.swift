//
// Created by liq7 on 12/28/16.
// Copyright (c) 2016 Pairboost. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import RxSwift
import RxCocoa
import Kingfisher

public struct HotVideoCellSize {
  static var imageHeight = CGFloat(150)
  static var imagePadding = Dimens.MATERIAL_SPACING_16
  static var titlePadding = Dimens.MATERIAL_SPACING_16
  static var titleFont = UIFont.systemFont(ofSize: UIFont.systemFontSize)
  static var timePadding = Dimens.MATERIAL_SPACING_8

  static func getSize(text:String, width:CGFloat) -> CGSize {
    let height = CGFloat(imagePadding) + imageHeight + text.heightForComment(font: titleFont, width: width) + CGFloat(
        titlePadding) * 2
    return CGSize(width: width, height: height)
  }

}

class HotVideoCell: UIKit.UICollectionViewCell {
  private var titleLabel:UILabel
  private var timeLabel:UILabelWithInset
  private var videoImageView:UIImageView
  private var resizeProcessor:ResizingCropCenterImageProcessor


  override init(frame:CGRect) {
    titleLabel = UILabel()
    titleLabel.theme_textColor = Themes.themedTextColorLight
    timeLabel = UILabelWithInset(CGRect.zero, top: 2.0, left: 5.0, bottom: 2.0, right: 5.0)
    timeLabel.backgroundColor = UIColor.black
    timeLabel.textColor = Themes.lightTextColor
    videoImageView = UIImageView()
    resizeProcessor = ResizingCropCenterImageProcessor(targetSize: CGSize(width: UIScreen.main.bounds.size.width,
        height: 150))
    super.init(frame: frame)
    contentView.theme_backgroundColor = Themes.backgroundColorCell
    contentView.addSubview(videoImageView)
    contentView.addSubview(titleLabel)
    contentView.addSubview(timeLabel)

    videoImageView.snp.makeConstraints { (make) -> Void in
      make.top.equalToSuperview().offset(HotVideoCellSize.imagePadding)
      make.left.equalToSuperview().offset(HotVideoCellSize.imagePadding)
      make.right.equalToSuperview().offset(-HotVideoCellSize.imagePadding)
      make.height.equalTo(HotVideoCellSize.imageHeight)
    }

    titleLabel.snp.makeConstraints { (make) -> Void in
      make.top.equalTo(videoImageView.snp.bottom).offset(HotVideoCellSize.titlePadding)
      make.left.equalToSuperview().offset(HotVideoCellSize.titlePadding)
      make.right.equalToSuperview().offset(-HotVideoCellSize.titlePadding)
      make.bottom.equalToSuperview().offset(-HotVideoCellSize.titlePadding)
    }

    timeLabel.snp.makeConstraints { (make) -> Void in
      make.trailing.equalTo(videoImageView).offset(-HotVideoCellSize.timePadding)
      make.bottom.equalTo(videoImageView).offset(-HotVideoCellSize.timePadding)
    }

  }

  required init(coder aDecoder:NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }


  func update(_ dto:VideoHotDto) {
    titleLabel.text = dto.videoTitle
    timeLabel.text = dto.duration
    videoImageView.kf.setImage(with: URL(string: dto.thumbUrl))
  }
}
