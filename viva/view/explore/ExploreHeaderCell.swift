//
// Created by liq7 on 1/3/17.
// Copyright (c) 2017 Pairboost. All rights reserved.
//

import Foundation
import SnapKit
import UIKit
import SwiftTheme

public struct ExploreHeaderSize {
  static var titleFont = UIFont.systemFont(ofSize: UIFont.systemFontSize)
  static var padding = Dimens.MATERIAL_SPACING_16

  static func getSize(text:String, width:CGFloat) -> CGSize {
    let height = CGFloat(padding) + text.heightForComment(font: titleFont, width: width) + CGFloat(padding)
    return CGSize(width: width, height: height)
  }
}

class ExploreHeaderCell: UIKit.UICollectionViewCell {
  private var titleLabel:UILabel

  override init(frame:CGRect) {
    titleLabel = UILabel()
    titleLabel.theme_textColor = Themes.themedTextColorLight

    super.init(frame: frame)
    contentView.theme_backgroundColor = Themes.backgroundColorCell
    contentView.addSubview(titleLabel)

    titleLabel.snp.makeConstraints { (make) -> Void in
      make.centerY.equalToSuperview()
      make.leading.equalToSuperview().offset(ExploreHeaderSize.padding)
      make.trailing.equalToSuperview().offset(-ExploreHeaderSize.padding)
    }
  }

  required init(coder aDecoder:NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func update(_ dto:ExploreHeader) {
    titleLabel.text = dto.title
  }


}
