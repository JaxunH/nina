//
// Created by liq7 on 1/3/17.
// Copyright (c) 2017 Pairboost. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class CategoryPillCell: UICollectionViewCell {
  private var categoryLabel:UILabelWithInset

  override init(frame:CGRect) {
    categoryLabel = UILabelWithInset(CGRect.zero, top: 2.0, left: 5.0, bottom: 2.0, right: 5.0)
    super.init(frame: frame)

    categoryLabel.font = UIFont.preferredFont(forTextStyle: .footnote)
    self.contentView.addSubview(categoryLabel)
    categoryLabel.snp.makeConstraints({ (make) -> Void in
      make.margins.equalToSuperview()
    })
    self.backgroundColor = UIColor.clear

  }

  required init?(coder aDecoder:NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func update(_ dto:CategoryDto, selected:Bool) {
//    NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
//    myLabel.attributedText = [[NSAttributedString alloc] initWithString:@"Test string"
//    attributes:underlineAttribute];
  let textColor = (selected ? UIColor.purple : UIColor.brown)
    let attribute:[String : Any] = [NSForegroundColorAttributeName: textColor]
    let attributeText = NSMutableAttributedString(string: dto.categoryTitle, attributes: attribute)
    let stringRange = NSMakeRange(0, attributeText.length)
    attributeText.beginEditing()
    attributeText.addAttribute(NSUnderlineStyleAttributeName, value: 1, range: stringRange)
    attributeText.endEditing()
    categoryLabel.attributedText = attributeText
  }

}