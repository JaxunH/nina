//
// Created by liq7 on 1/3/17.
// Copyright (c) 2017 Pairboost. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

protocol SelectCategoryDelegate{
  func selectCategory(_ categoryDto: CategoryDto) -> Void
}

class CategoryPillCollectionView: UIView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

  private var collectionView:UICollectionView
  private var categoryDtos:[CategoryDto]
  private var selectedCategory:CategoryDto?
  private var pillCellForSize:CategoryPillCell
  var selectCategoryDelegate:SelectCategoryDelegate?

  override init(frame:CGRect) {
    let flowLayout:UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
    flowLayout.scrollDirection = UICollectionViewScrollDirection.horizontal
    collectionView = UICollectionView(frame: CGRect(), collectionViewLayout: flowLayout)
    categoryDtos = []
    pillCellForSize = CategoryPillCell()
    selectCategoryDelegate = nil
    super.init(frame: frame)

    collectionView.register(CategoryPillCell.self, forCellWithReuseIdentifier: NSStringFromClass(CategoryPillCell.self))
    addSubview(collectionView)
    collectionView.snp.makeConstraints({ (make) -> Void in
      make.top.leading.equalToSuperview().offset(Dimens.MATERIAL_SPACING_8)
      make.bottom.trailing.equalToSuperview().offset(-Dimens.MATERIAL_SPACING_8)
      make.height.greaterThanOrEqualTo(Dimens.MATERIAL_SPACING_24)
    })
    collectionView.delegate = self
    collectionView.dataSource = self
    self.backgroundColor = UIColor.clear
    self.collectionView.backgroundColor = UIColor.clear
  }

  required init?(coder aDecoder:NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func collectionView(_ collectionView:UICollectionView, numberOfItemsInSection section:Int) -> Int {
    return self.categoryDtos.count
  }

  func collectionView(_ collectionView:UICollectionView, cellForItemAt indexPath:IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(CategoryPillCell.self), for: indexPath) as! CategoryPillCell
    cell.update(categoryDtos[indexPath.row], selected: categoryDtos[indexPath.row] == selectedCategory)
    return cell
  }

  func collectionView(_ collectionView:UICollectionView, layout collectionViewLayout:UICollectionViewLayout, sizeForItemAt indexPath:IndexPath) -> CGSize {

    pillCellForSize.update(categoryDtos[indexPath.row], selected: false)

    return CGSize(width: pillCellForSize.estimateWidth(), height: pillCellForSize.estimateHeight())
  }

  func setCategories(_ categories:[CategoryDto]) {
    self.categoryDtos = categories
    collectionView.reloadData()
  }

  func collectionView(_ collectionView:UICollectionView, didSelectItemAt indexPath:IndexPath) {
    selectCategoryDelegate?.selectCategory(categoryDtos[indexPath.row])
    selectedCategory = categoryDtos[indexPath.row]
    collectionView.reloadData()
  }

  func resetCategory() {
    selectedCategory = nil
    collectionView.reloadData()
  }


}
