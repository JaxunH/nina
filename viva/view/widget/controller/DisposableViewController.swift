//
// Created by ingram on 1/6/17.
// Copyright (c) 2017 Pairboost. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import ObjectiveC

protocol DisposableViewController {
  var disposeBag:DisposeBag { get }
}

extension UIViewController: DisposableViewController {

  private struct AssociatedKeys {
    static var disposeBag = "viva_disposeBag"
  }

  var disposeBag:DisposeBag {
    get {
      if let bag = objc_getAssociatedObject(self, &AssociatedKeys.disposeBag) as? DisposeBag {
        return bag
      }
      let newBag = DisposeBag()
      objc_setAssociatedObject(
          self,
          &AssociatedKeys.disposeBag,
          newBag,
          objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
      return newBag
    }
  }
}
