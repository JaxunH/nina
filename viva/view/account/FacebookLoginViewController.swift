//
// Created by ingram on 1/3/17.
// Copyright (c) 2017 Pairboost. All rights reserved.
//

import Foundation
import UIKit
import FacebookLogin
import FacebookCore
import RxSwift
import RxCocoa
import TTGSnackbar
import SwiftTheme

class FacebookLoginViewController: UIViewController, LoginButtonDelegate {

  let facebookDaemon:FacebookDaemon = Injector.inject()
  let finishSubject = PublishSubject<Bool>()

  class func create(_ present:(FacebookLoginViewController) -> ()) -> Observable<Bool> {
    let accountDaemon:AccountDaemon = Injector.inject()
    if accountDaemon.isRegistered {
      return Observable.just(true)
    }
    let controller = FacebookLoginViewController()
    present(controller);
    return controller.finishSubject.asObservable()
  }

  deinit {
    finishSubject.dispose()
  }

  required override init?(coder aDecoder:NSCoder) {
    super.init(coder: aDecoder)
  }

  required override init(nibName nibNameOrNil:String?, bundle nibBundleOrNil:Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    let loginButton = LoginButton(readPermissions: facebookDaemon.readPermissions)
    loginButton.center = view.center
    loginButton.delegate = self
    view.addSubview(loginButton)
  }

  func loginButtonDidCompleteLogin(_ loginButton:LoginButton, result:LoginResult) {
    if let accessToken = AccessToken.current {
      log.debug("fb user authorized: \(accessToken)")

      //TODO loading indicator
      facebookDaemon.register(accessToken)
          .subscribe(onNext: { [weak self] account in
            guard let safeSelf = self else {
              return;
            }
            safeSelf.quit(success: true)
          }, onError: { [weak self] error in
            guard let safeSelf = self else {
              return;
            }
            //TODO i18n registration error message
            safeSelf.showErrorToast("\(error)")
            safeSelf.quit(success: false)
          }).addDisposableTo(disposeBag)
      // User is logged in, use 'accessToken' here.
    } else {
      log.warn("facebook access token not available")
      showErrorToast("TODO facebook access token not available")
      quit(success: false)
    }
  }

  private func quit(success:Bool) {
    finishSubject.onNext(success)
    finishSubject.onCompleted()
    dismiss(animated: false)
  }

  private func showErrorToast(_ msg:String) {
    let bar = TTGSnackbar.init(message: msg, duration: .middle)
    //TODO apply theme color
    bar.theme_backgroundColor = Themes.backgroundColor
    bar.show()
  }

  func loginButtonDidLogOut(_ loginButton:LoginButton) {
    log.warn("facebook log out")
    quit(success: false)
  }

}
