//
// Created by liq7 on 1/4/17.
// Copyright (c) 2017 Pairboost. All rights reserved.
//

import Foundation
import AVFoundation
import AVKit
import RxSwift
import TTGSnackbar
import UIKit
import BMPlayer

class VivaAVPlayerViewController: AVPlayerViewController {
  private var myContext = 0
  private var contextLoadedTimeRanges = 1
  private var videoId:Int!
  private let videoDaemon:VideoDaemon = Injector.inject()

  required init?(coder aDecoder:NSCoder) {
    super.init(coder: aDecoder)
  }

  init(videoId:Int) {
    super.init(nibName: nil, bundle: nil)
    self.videoId = videoId
  }

  override func viewDidLoad() {
    print("player view did load")

    videoDaemon.getVideo(self.videoId).subscribe(onNext: { [weak weakSelf = self] videoDto in
          guard let strongSelf = weakSelf else {
            return
          }
          guard let url = URL(string: videoDto.streamingInfo.hls) else {
            return
          }
//          strongSelf.play(url: url)
        strongSelf.play(url: url)
        }).addDisposableTo(disposeBag)
  }


  func play(url:URL) {
    player = AVPlayer(url: url)

    if let player = player {
      player.addObserver(self, forKeyPath: "status", context: &myContext)
    }

    if let item:AVPlayerItem = self.player?.currentItem {
      item.addObserver(self, forKeyPath: "loadedTimeRanges", context: &contextLoadedTimeRanges)
    }
  }

  @objc
  override open func observeValue(forKeyPath keyPath:String?,
      of object:Any?,
      change:[NSKeyValueChangeKey: Any]?,
      context:UnsafeMutableRawPointer?) {
    if context == &myContext {
      print("change \(change)")
    } else if context == &contextLoadedTimeRanges {
      if let item:AVPlayerItem = self.player?.currentItem {
        print("buffer status \(item.loadedTimeRanges)")
      }
    } else {
      super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
    }
  }

  deinit {
    if let p:AVPlayer = self.player {
      p.removeObserver(self, forKeyPath: "status")
    }
    if let item:AVPlayerItem = self.player?.currentItem {
      item.removeObserver(self, forKeyPath: "loadedTimeRanges")
    }
  }

}
