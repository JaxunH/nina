//
// Created by liq7 on 1/4/17.
// Copyright (c) 2017 Pairboost. All rights reserved.
//

import Foundation
import AVFoundation
import AVKit
import RxSwift
import TTGSnackbar
import UIKit
import BMPlayer

class VivaBMPlayerViewController: UIViewController {
  private var myContext = 0
  private var contextLoadedTimeRanges = 1
  private var videoId:Int!
  private var videoTitle:String!
  private let videoDaemon:VideoDaemon = Injector.inject()

  required init?(coder aDecoder:NSCoder) {
    super.init(coder: aDecoder)

  }

  init(videoHotDto:VideoHotDto) {
    super.init(nibName: nil, bundle: nil)
    self.videoId = videoHotDto.videoId
    self.videoTitle = videoHotDto.videoTitle
  }

  override func viewDidLoad() {
    print("player view did load")

    videoDaemon.getVideo(self.videoId).subscribe(onNext: { [weak weakSelf = self] videoDto in
          guard let strongSelf = weakSelf else {
            return
          }
          guard let url = URL(string: videoDto.streamingInfo.hls) else {
            return
          }
//          strongSelf.play(url: url)
          strongSelf.playWithBMPlayer(url: url)
        }).addDisposableTo(disposeBag)
  }


  func playWithBMPlayer(url:URL) {
    let controlView:CustomBMPlayerControlView = CustomBMPlayerControlView()
    let playerView:BMPlayer = BMPlayer(customControllView: controlView)

    view.addSubview(playerView)
    playerView.snp.makeConstraints { (make) in
      make.top.bottom.equalTo(self.view).offset(0)
      make.left.right.equalTo(self.view)
      // Note here, the aspect ratio 16:9 priority is lower than 1000 on the line, because the 4S iPhone aspect ratio is not 16:9
//      make.height.equalTo(playerView.snp.width).multipliedBy(9.0/16.0).priority(750)

    }
// Back button event
    playerView.backBlock = { (b) -> Void in
      let _ = self.navigationController?.popViewController(animated: true)
      self.dismiss(animated: true)
    }

    playerView.playWithURL(URL(string: url.absoluteString)!, title: videoTitle)
    controlView.playerTitleLabel2?.text = "60分鐘....."

  }


}
