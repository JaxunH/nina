//
// Created by ingram on 12/28/16.
// Copyright (c) 2016 Pairboost. All rights reserved.
//

import Foundation
import RxSwift
import RxAlamofire
import Alamofire

typealias ApiErrorHandler = (ApiError) -> Void

class BaseService {

  fileprivate let pref:Pref

  static var apiErrorHandler:ApiErrorHandler = {
    print($0)
  }

  init(_ pref:Pref) {
    self.pref = pref
  }
}

enum ApiError: Swift.Error, Equatable {
  case resourceNotFoundError
  case authenticationError
  case serviceError
  case internalServerError
  case networkError
  case httpStatusError
  case programBugError(AFError)

  static func convertError(_ error:Swift.Error) -> ApiError {
    guard let afError = (error as? AFError) else {
      return networkError
    }
    guard let responseCode = afError.responseCode else {
      return programBugError(afError)
    }
    if (responseCode == 400) {
      return serviceError
    } else if (responseCode == 401 || responseCode == 403) {
      return authenticationError
    } else if (responseCode == 404) {
      return resourceNotFoundError
    } else if (responseCode >= 500 && responseCode < 600) {
      return internalServerError
    } else {
      return httpStatusError
    }
  }

  static func ==(lhs:ApiError, rhs:ApiError) -> Bool {
    switch (lhs, rhs) {
    case (.programBugError, .programBugError):
      return true
    case (.serviceError, .serviceError):
      return true
    case (.resourceNotFoundError, .resourceNotFoundError):
      return true
    case (.authenticationError, .authenticationError):
      return true
    case (.internalServerError, .internalServerError):
      return true
    case (.networkError, .networkError):
      return true
    case (.httpStatusError, .httpStatusError):
      return true
    default:
      return false
    }
  }
}

// MARK: mapper

extension BaseService {

  func dtoArrayMapper<T:JsonDto>(_ transform:@escaping ([String: Any?]) -> T) -> (Any) -> Array<T> {
    return { (jsonArray) -> Array<T> in
      return (jsonArray as! Array).map {
        transform($0)
      }
    }
  }

  func pagerMapper<T:JsonDto>(_ name:String,
      _ transform:@escaping ([String: Any?]) -> T) -> (Any) -> Array<T> {

    let pageDtoMapper = dtoMapper(PagerDto.fromDictionary)
    let embeddedArrayMapper = dtoArrayMapper(transform)

    return { (pagerJson) -> Array<T> in
      let pagerDto = pageDtoMapper(pagerJson)
      let jsonArray = pagerDto.resolveEmbeddedArray(name)
      return embeddedArrayMapper(jsonArray)
    }
  }

  func dtoMapper<T:JsonDto>(_ transform:@escaping ([String: Any?]) -> T) -> (Any) -> T {
    return { (json) -> T in
      return transform(json as! [String: Any?])
    }
  }

  func voidMapper() -> (Any) -> Void {
    return { (any) -> Void in
      return ()
    }
  }

  ///
  /// handle all network error and api error and consume to empty result (onNext: won't triggered)
  ///
  /// note that status 404 will fallback to default if provided
  ///
  fileprivate func consumeIfApiError<T>(_ resourceNotFoundDefault:Any?) -> (Swift.Error) -> Observable<T> {
    return { (error:Swift.Error) -> Observable<T> in
      let apiError = ApiError.convertError(error)
      switch (apiError) {
      case .resourceNotFoundError:
        if let defaultValue = resourceNotFoundDefault {
          return Observable.just(defaultValue as! T)
        } else {
          log.error("api error - \(error)")
          BaseService.apiErrorHandler(apiError)
          return Observable.empty()
        }
      default:
        log.error("api error - \(error)")
        BaseService.apiErrorHandler(apiError)
        return Observable.empty()
      }
    }
  }
}

// MARK: json request

extension BaseService {

  private static var endpoint:String {
    return BuildConfig.ottEndpoint;
  }

  private func defaultHeaders() -> [String: String] {
    var headers = ["OTT-HOST": BuildConfig.ottBrand]
    if let ottToken = pref.ottToken {
      headers["OTT-TOKEN"] = ottToken
    }
    return headers;
  }

  private static func cleanParams(_ params:[String: Any?]?) -> [String: Any]? {
    guard let params = params else {
      return nil
    }
    var cleanParams:[String: Any] = [:]
    for (key, value) in params {
      if (value != nil) {
        cleanParams[key] = value!;
      }
    }
    return cleanParams
  }

  private func jsonOperation(method:HTTPMethod,
      encoding:ParameterEncoding,
      path:String,
      parameters:[String: Any?]?,
      resourceNotFoundDefault:Any?) -> Observable<Any> {
    return json(method,
        "\(BaseService.endpoint)\(path)",
        parameters: BaseService.cleanParams(parameters),
        encoding: encoding,
        headers: defaultHeaders())
        .observeOn(MainScheduler.instance)
        .catchError(consumeIfApiError(resourceNotFoundDefault))
  }

  private func requestOperation(method:HTTPMethod,
      encoding:ParameterEncoding,
      path:String,
      parameters:[String: Any?]?,
      ignoreResourceNotFoundDefault:Bool) -> Observable<Void> {
    return request(method,
        "\(BaseService.endpoint)\(path)",
        parameters: BaseService.cleanParams(parameters),
        encoding: encoding,
        headers: defaultHeaders())
        .map { (dataRequest) in
          return ()
        }
        .observeOn(MainScheduler.instance)
        .catchError(consumeIfApiError(ignoreResourceNotFoundDefault ? () : nil))
  }

  func jsonGet(_ path:String, params:[String: Any?]? = nil, resourceNotFoundDefault:Any? = nil) -> Observable<Any> {
    return jsonOperation(method: .get,
        encoding: URLEncoding.default,
        path: path,
        parameters: params,
        resourceNotFoundDefault: resourceNotFoundDefault)
  }

  ///
  /// post and handle response as json (if response is empty content, use post() instead)
  ///
  func jsonPost(_ path:String, body:[String: Any?]? = nil, resourceNotFoundDefault:Any? = nil) -> Observable<Any> {
    return jsonOperation(method: .post,
        encoding: JSONEncoding.default,
        path: path,
        parameters: body,
        resourceNotFoundDefault: resourceNotFoundDefault)
  }

  func post(_ path:String, body:[String: Any?]? = nil, ignoreResourceNotFound:Bool) -> Observable<Void> {
    return requestOperation(method: .post,
        encoding: JSONEncoding.default,
        path: path,
        parameters: body,
        ignoreResourceNotFoundDefault: ignoreResourceNotFound)
  }

  ///
  /// delete and handle response as json (if response is empty content, use delete() instead)
  ///
  func jsonDelete(_ path:String, body:[String: Any?]? = nil, resourceNotFoundDefault:Any? = nil) -> Observable<Any> {
    return jsonOperation(method: .delete,
        encoding: JSONEncoding.default,
        path: path,
        parameters: body,
        resourceNotFoundDefault: resourceNotFoundDefault)
  }

  func delete(_ path:String, body:[String: Any?]? = nil, ignoreResourceNotFound:Bool) -> Observable<Void> {
    return requestOperation(method: .delete,
        encoding: JSONEncoding.default,
        path: path,
        parameters: body,
        ignoreResourceNotFoundDefault: ignoreResourceNotFound)
  }

  ///
  /// put and handle response as json (if response is empty content, use put() instead)
  ///
  func jsonPut(_ path:String, body:[String: Any?]? = nil, resourceNotFoundDefault:Any? = nil) -> Observable<Any> {
    return jsonOperation(method: .put,
        encoding: JSONEncoding.default,
        path: path,
        parameters: body,
        resourceNotFoundDefault: resourceNotFoundDefault)
  }

  func put(_ path:String, body:[String: Any?]? = nil, ignoreResourceNotFound:Bool) -> Observable<Void> {
    return requestOperation(method: .put,
        encoding: JSONEncoding.default,
        path: path,
        parameters: body,
        ignoreResourceNotFoundDefault: ignoreResourceNotFound)
  }
}
