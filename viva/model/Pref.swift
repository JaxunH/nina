//
// Created by ingram on 12/28/16.
// Copyright (c) 2016 Pairboost. All rights reserved.
//

import Foundation

class Pref {

  var ottToken:String? {
    get {
      return UserDefaults.standard.value(forKey: "ottToken") as! String?
    }
    set(token) {
      UserDefaults.standard.setValue(token, forKey: "ottToken")
    }
  }

  var isRegistered:Bool {
    return ottToken != nil
  }
}
